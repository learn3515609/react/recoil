import React from 'react';
import { Form, Input, Button } from 'antd';
import { useForm } from 'react-hook-form';
import { useRecoilState, useRecoilValue } from 'recoil';
import { formState, formStatePassSelector } from './state';

function App() {
  const [formValue, setFormValue] = useRecoilState(formState);
  const password = useRecoilValue(formStatePassSelector);

  const {
    register,
    handleSubmit,
    reset,
    resetField,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    defaultValues: formValue
  });

  const onSubmit = (data: typeof formValue) => {
    setFormValue(data);
  }

  const onReset = () => {
    reset();
  }

  console.log(formValue);

  return (
    <div className="App">

      <Form style={{width: 500}}>
        <Form.Item label="Login">
          <input placeholder="login" autoComplete="off" {...register('login')} />
        </Form.Item>
        <Form.Item label="Password">
          <input autoComplete="off" {...register('password', {required: {value: true, message: 'Пустой пароль'}, minLength: {value: 3, message: 'Короткий пароль'}})} />
          {errors.password && <div style={{color: 'red'}}>{errors.password.message}</div>}
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" onClick={handleSubmit(onSubmit)} style={{marginRight: 8}} >
            Submit
          </Button>
          <Button onClick={onReset}>
            Reset
          </Button>
        </Form.Item>

        <div><h3>Password from state: </h3> {password}</div>
      </Form>
    </div>
  );
}

export default App;
