import { atom, selector } from 'recoil';

type TFormState = {
  login: string;
  password: string;
}

export const formState = atom<TFormState>({
  key: 'formState',
  default: {
    login: '',
    password: ''
  }
});

export const formStatePassSelector = selector({
  key: 'formStatePassSelector',
  get: ({get}) => {
    const form = get(formState);
    return form.password;
  },
});